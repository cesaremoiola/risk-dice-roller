package com.example.diceroller

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageView

class MainActivity : AppCompatActivity() {

    private var numberOfAttacker = 3
    private var numberOfDefender = 3

    // images
    private lateinit var diceAttacker: Array<ImageView>
    private lateinit var diceDefender: Array<ImageView>

    // buttons
    private lateinit var addAttackerButton: Button
    private lateinit var subAttackerButton: Button
    private lateinit var addDefenderButton: Button
    private lateinit var subDefenderButton: Button
    private lateinit var rollButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_main)

        // get images
        diceAttacker = arrayOf(findViewById(R.id.imageView1),findViewById(R.id.imageView2),findViewById(R.id.imageView3))
        diceDefender = arrayOf(findViewById(R.id.imageView4),findViewById(R.id.imageView5),findViewById(R.id.imageView6))

        // set colors
        for(i in (diceDefender.indices)){

            diceAttacker[i].setColorFilter(Color.rgb(255,93,93))
            diceDefender[i].setColorFilter(Color.rgb(93,155,255))
        }

        // get buttons
        addAttackerButton = findViewById(R.id.add_attacker)
        subAttackerButton = findViewById(R.id.sub_attacker)
        addDefenderButton = findViewById(R.id.add_defender)
        subDefenderButton= findViewById(R.id.sub_defender)
        rollButton= findViewById(R.id.roll_button)

        // set listeners
        addAttackerButton.setOnClickListener { addAttacker() }
        subAttackerButton.setOnClickListener { subAttacker() }
        addDefenderButton.setOnClickListener { addDefender() }
        subDefenderButton.setOnClickListener { subDefender() }
        rollButton.setOnClickListener { roll() }
    }

    private fun addAttacker(){
        if(numberOfAttacker<3) numberOfAttacker++
        if(numberOfAttacker>2) addAttackerButton.isEnabled=false
        if(numberOfAttacker>1) subAttackerButton.isEnabled=true
        setDices()
    }
    private fun subAttacker(){
        if(numberOfAttacker>1) numberOfAttacker--
        if(numberOfAttacker<3) addAttackerButton.isEnabled=true
        if(numberOfAttacker<2) subAttackerButton.isEnabled=false
        setDices()
    }
    private fun addDefender(){
        if(numberOfDefender<3) numberOfDefender++
        if(numberOfDefender>2) addDefenderButton.isEnabled=false
        if(numberOfDefender>1) subDefenderButton.isEnabled=true
        setDices()
    }
    private fun subDefender(){
        if(numberOfDefender>1) numberOfDefender--
        if(numberOfDefender<3) addDefenderButton.isEnabled=true
        if(numberOfDefender<2) subDefenderButton.isEnabled=false
        setDices()
    }
    private fun setDices(){
        for(i in diceAttacker.indices){
            if(i<numberOfAttacker) diceAttacker[i].alpha=1.0f
            else diceAttacker[i].alpha=0.0f
        }
        for(i in diceDefender.indices){
            if(i<numberOfDefender) diceDefender[i].alpha=1.0f
            else diceDefender[i].alpha=0.0f
        }
    }
    private fun roll(){
        Log.d("MainActivity", "Roll")

        //Roll dices
        val attackerResults: ArrayList<Int> = ArrayList()
        val defenderResults: ArrayList<Int> = ArrayList()
        for (i in 1..numberOfAttacker){
            attackerResults.add((1..6).random())
        }
        for (i in 1..numberOfDefender){
            defenderResults.add((1..6).random())
        }
        attackerResults.sortDescending()
        defenderResults.sortDescending()

        Log.d("MainActivity", "Results:\nAttackers: $attackerResults\nDefenders: $defenderResults")

        //Show result
        for(i in (0 until numberOfAttacker)){
            diceAttacker[i].setImageResource(getFace(attackerResults[i]))
        }
        for(i in 0 until numberOfDefender){
            diceDefender[i].setImageResource(getFace(defenderResults[i]))
        }

        //Show winner
        for(i in 0 until numberOfAttacker){
            if(i < numberOfDefender){
                if(attackerResults[i] <= defenderResults[i]) {
                    diceAttacker[i].alpha=0.2f
                    diceDefender[i].alpha=1f
                }
                else {
                    diceAttacker[i].alpha=1f
                    diceDefender[i].alpha=0.2f
                }
            }
        }
    }

    private fun getFace(num: Int): Int {
        var imageId:Int = R.drawable.dice_1
        val faces: Array<Int> = arrayOf(
            R.drawable.dice_1,
            R.drawable.dice_2,
            R.drawable.dice_3,
            R.drawable.dice_4,
            R.drawable.dice_5,
            R.drawable.dice_6
        )
        if(num in 1..6) imageId = faces[num-1]
        return imageId
    }
}
